//specification that players would be interested to have (covering them from angles of different interested parties so it doesn't cover only the contract, but the whole imagined system. the result is dependent on the whole system):
// - if I send some money to the contract, I will either become the new king or I will get the money back (minus gas expenses)
// - if I send amount of money required from the contract, I will become the new king **this is actually not true due to reordering issues, but might be a reasonable expectation by a user interested in becoming the new king**
// - if I payed x and became the king, I will eventually get 1.5x - 0.01*1.5x or I will rule forever  **this is also not true due to known bug with not enough gas. 
// in order to check it one needs to see the contract that asks for the money as well** (https://www.kingoftheether.com/postmortem.html)
// - wizard commision will only ever increase until it is payed out to the wizard and wizard will receive the money (in this case it is also not true, depends on the wizard address, but it's reasonable to assume it would hold
// since presumably wizard will not spend too much gas or increase the recursion depth)



// A chain-game contract that maintains a 'throne' which agents may pay to rule.
// (c) Kieran Elby. All rights reserved.
// Lives at 0xa9d160e32ad37ac6f2b8231e4efe14d35abb576e.
// This is the old v0.3.0 version - see kingoftheether.com for new version.
// Inspired by ethereumpyramid.com and the (now-gone?) "magnificent bitcoin gem".
// TODO - round amounts to e.g. 3 sig fig, start with higher amount.
// TODO - allow ownership transfer / creating new thrones?
// TODO - enforce time-limit on reign (can contracts do that without external action): time-limit was actually enforced in the kingOfEther safe version
// TODO - add bitcoin bridge so agents can pay in bitcoin?

//ivan: this is an old version of the contract (the current, final one, has much more possibilities, creations of different kingdoms etc. That one will be included once I understand this one properly)

pragma solidity ^0.4.14;
contract KingOfTheEtherThrone {

    struct Monarch {
        // Address from which they paid their claimFee
        // and to which their compensation will be sent.
        address etherAddress;
        // A name by which they wish to be known.
        // TODO - should I use string? bytes? bytes32?
        string name;
        // How much did they pay to become monarch?
        uint claimPrice;
        // When did their rule start (based on block.timestamp)?
        uint coronationTimestamp;
    }

    // The wizard is the hidden power behind the throne; they
    // occupy the throne during gaps in succession and collect fees.
    address wizardAddress;

    // We let the wizard payments build up rather than consume
    // too much gas.
    uint accumulatedWizardPayments = 0;

    // Used to ensure only the wizard can do some things.
	// function modifier is used to change the behavior of the functions outside of the functions themselves. http://solidity.readthedocs.io/en/develop/contracts.html#function-modifiers
	// useful to make sure a certain condition is met before starting with the function execution. usually, keyword require is used instead of if (it throws is the condition is not met)
    modifier onlywizard { if (msg.sender == wizardAddress) _; }

    // How much must the first monarch pay?
    uint constant startingClaimPrice = 10 finney;

    // The next claimPrice is calculated from the previous claimFee
    // by multiplying by claimFeeAdjustNum and dividing by claimFeeAdjustDen -
    // for example, num=3 and den=2 would cause a 50% increase.

	//there are no reals in solidity (they were mentioned in some old documentation and abandoned since)
    uint constant claimPriceAdjustNum = 3;
    uint constant claimPriceAdjustDen = 2;

    // How much of each claimFee goes to the wizard (expressed as a fraction)?
    // e.g. num=1 and den=100 would deduct 1% for the wizard, leaving 99% as
    // the compensation fee for the usurped monarch.
    uint constant wizardCommissionFractionNum = 1;
    uint constant wizardCommissionFractionDen = 100;

    // How much ether must an agent pay now to become the monarch?
    uint public currentClaimPrice;

    // The King (or Queen) of the Ether.
    Monarch public currentMonarch;

    // Earliest-first list of previous throne holders.
    Monarch[] public pastMonarchs;

    // Create a new throne, with the creator as wizard and first ruler.
    // Sets up some hopefully sensible defaults.
    function KingOfTheEtherThrone() {
        wizardAddress = msg.sender;
        currentClaimPrice = startingClaimPrice;
        currentMonarch = Monarch(
            wizardAddress,
            "[Vacant]",
            0,
            block.timestamp
        );
    }

    function numberOfMonarchs() constant returns (uint n) {
        return pastMonarchs.length;
    }

    // Fired when the throne is claimed. Can be used to help build a front-end.
	// events are used to keep track (inside the blockchain) of particular interesting situations (I'd write "events", but not in the same meaning as those events). they have to be explicitly called 
	// in the code and then the information they specify (here, address, name and new price) is added to the log in the blockchain. one can also define listeners for events and then react to them
    event ThroneClaimed(
        address usurperEtherAddress,
        string usurperName,
        uint newClaimPrice
    );

    // Fallback function - simple transactions trigger this.
    // Assume the message data is their desired name.
	//this will get triggered when the contract receives any simple transaction. how does this work connected to new gas limitations? check this out
    function() {
        claimThrone(string(msg.data));
    }

    // Claim the throne for the given name by paying the currentClaimFee.
    function claimThrone(string name) payable {

        uint valuePaid = msg.value;

        // If they paid too little, reject claim and refund their money.
        if (valuePaid < currentClaimPrice) {

			// interesting there is no checking whether this suceeds or not... how problematic is that? what are the consequences
            msg.sender.send(valuePaid);
            return;
        }

        // If they paid too much, continue with claim but refund the excess.
        if (valuePaid > currentClaimPrice) {
            uint excessPaid = valuePaid - currentClaimPrice;
            msg.sender.send(excessPaid);
            valuePaid = valuePaid - excessPaid;
        }

        // The claim price payment goes to the current monarch as compensation
        // (with a commission held back for the wizard). We let the wizard's
        // payments accumulate to avoid wasting gas sending small fees.

        uint wizardCommission = (valuePaid * wizardCommissionFractionNum) / wizardCommissionFractionDen;
        accumulatedWizardPayments += wizardCommission;

        uint compensation = valuePaid - wizardCommission;

        if (currentMonarch.etherAddress != wizardAddress) {

			//again, no checking for failure here
            currentMonarch.etherAddress.send(compensation);
        } else {
            // When the throne is vacant, the wizard is paid the fee.
            accumulatedWizardPayments += compensation;
        }

        // Usurp the current monarch, replacing them with the new one.
        pastMonarchs.push(currentMonarch);
        currentMonarch = Monarch(
            msg.sender,
            name,
            valuePaid,
            block.timestamp
        );

        // Increase the claim fee for next time.
        currentClaimPrice = currentClaimPrice * claimPriceAdjustNum / claimPriceAdjustDen;

        // Hail the new monarch!
		// firing event ThroneClaimed defined earlier
        ThroneClaimed(currentMonarch.etherAddress, currentMonarch.name, currentClaimPrice);
    }

    // Used only by the wizard to collect his commission.
	// using the *onlywizard* modifier here
    function sweepCommission() onlywizard {
        if (accumulatedWizardPayments == 0) {
            return;
        }
		// again, no checking of the success of transaction
        wizardAddress.send(accumulatedWizardPayments);
        accumulatedWizardPayments = 0;
    }

}