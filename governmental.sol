// some list of requirements that investors in this government could have - either creditors or corruptElite. Not all of them are always satisfied, but I am writing them down as examples of what people could have in mind
// when considering investing into this contract (lending money to the government presented)
// if I lend x money to the government, I will eventually get 1.1x back
// if I lend x money to the government, I will either get 1.1x back or the government would collapse
// if I am the last investor into government before the collapse, I will get back more than I invested
// if I am the last investor into government beofre the collapse, I will get back some money  (this again depends on the accepting fallback functions and the whole system. particularly, here corrupt elite can call lendGovernment
// after 12 hours has passed with call stack depth of 1023, making sure that the subsequent send instruction fails, everything is cleared out and in the next call they collect all the money)
// another problem that actually happened is the fact that initializing the arrays to zeros is internally implemented with iterations over arrays and caused spending more gas than ther was available so 1100 ETH was stuck
// https://www.reddit.com/r/ethereum/comments/4ghzhv/governmentals_1100_eth_jackpot_payout_is_stuck/
// if I am the corrupt elite, I will never loose any money
// if I am the corrupt elite, I will get 5% of any accepted credit
// if 12 hours pass before the new credit is given, the government would collapse and the last creditor will be given all the profitFromCash  *problematic due to possible time manipulation and due to approximate caluclations of 12 hrs*
// if I send some money to the government before 12 hours pass, it won't fail and the system will continue working *again, problematic due to possible timestamp manipulation by miners*
// this contract will never try to send money that it doesn't posses


contract Government {

    // Global Variables
    uint32 public lastCreditorPayedOut;
    uint public lastTimeOfNewCredit;
    uint public profitFromCrash;
    address[] public creditorAddresses;
    uint[] public creditorAmounts;
    address public corruptElite;

	//mapping is always by default initialized to 0
    mapping (address => uint) buddies;
    uint constant TWELVE_HOURS = 43200;
    uint8 public round;

    function Government() {
        // The corrupt elite establishes a new government
        // this is the commitment of the corrupt Elite - everything that can not be saved from a crash
        profitFromCrash = msg.value;
        corruptElite = msg.sender;
        lastTimeOfNewCredit = block.timestamp;
    }

    function lendGovernmentMoney(address buddy) returns (bool) {
        uint amount = msg.value;
        // check if the system already broke down. If for 12h no new creditor gives new credit to the system it will brake down.
        // 12h are on average = 60*60*12/12.5 = 3456

		// there is no way how time passing can trigger the function, so the contract is using any next lending (in principle, currupt government can go trigger it if it wishes)
        if (lastTimeOfNewCredit + TWELVE_HOURS < block.timestamp) {
            // Return money to sender
            msg.sender.send(amount);
            // Sends all contract money to the last creditor
            creditorAddresses[creditorAddresses.length - 1].send(profitFromCrash);
			
			// balance is current amount of money that the Government contract has
            corruptElite.send(this.balance);
            // Reset contract state
            lastCreditorPayedOut = 0;
            lastTimeOfNewCredit = block.timestamp;
            profitFromCrash = 0;
            creditorAddresses = new address[](0);
            creditorAmounts = new uint[](0);
            round += 1; // if the whole system fails, the only thing that happens is that we're advancing to the new round
            return false;
        }
        else {
            // the system needs to collect at least 1% of the profit from a crash to stay alive
			// amount has to be at least one ether (10**18 wei)
            if (amount >= 10 ** 18) {
                // the System has received fresh money, it will survive at leat 12h more
				//is the manipulation with timestamps possible here? probably not, the miner can usually mess with it up to at most 900 seconds. however, if it is close to 12 hours, a miner can set 
				//timestamp to what he prefers (depending on whether it is better for him to crash or not to crash this government)
                lastTimeOfNewCredit = block.timestamp;
                // register the new creditor and his amount with 10% interest rate
                creditorAddresses.push(msg.sender);
                creditorAmounts.push(amount * 110 / 100);
                // now the money is distributed
                // first the corrupt elite grabs 5% - thieves!
                corruptElite.send(amount * 5/100);
                // 5% are going into the economy (they will increase the value for the person seeing the crash comming)
                if (profitFromCrash < 10000 * 10**18) {
                    profitFromCrash += amount * 5/100;
                }
                // if you have a buddy in the government (and he is in the creditor list) he can get 5% of your credits.
                // Make a deal with him.
				// send some money to a buddy if his "buddies value" (still not clear what that means) is above the credit (he gets this immediately: I guess that is the incentive for people to bring new people to the whole system)
                if(buddies[buddy] >= amount) {
                    buddy.send(amount * 5/100);
                }
				//now the sender is also added to the list of buddies ? the buddies concept seems a bit weird (why aren't creditors used directly?)
                buddies[msg.sender] += amount * 110 / 100;
                // 90% of the money will be used to pay out old creditors
				//but only one creditor is payed out and only if there is enough money in the bank
                if (creditorAmounts[lastCreditorPayedOut] <= address(this).balance - profitFromCrash) {
                    creditorAddresses[lastCreditorPayedOut].send(creditorAmounts[lastCreditorPayedOut]);
                    buddies[creditorAddresses[lastCreditorPayedOut]] -= creditorAmounts[lastCreditorPayedOut];
                    lastCreditorPayedOut += 1;
                }
                return true;
            }
            else {
                msg.sender.send(amount);
                return false;
            }
        }
    }

    // fallback function
    function() {
        lendGovernmentMoney(0);
    }

    function totalDebt() returns (uint debt) {
        for(uint i=lastCreditorPayedOut; i<creditorAmounts.length; i++){
            debt += creditorAmounts[i];
        }
    }

    function totalPayedOut() returns (uint payout) {
        for(uint i=0; i<lastCreditorPayedOut; i++){
            payout += creditorAmounts[i];
        }
    }

    // better don't do it (unless you are the corrupt elite and you want to establish trust in the system)
    function investInTheSystem() {
        profitFromCrash += msg.value;
    }

    // From time to time the corrupt elite inherits it's power to the next generation
    function inheritToNextGeneration(address nextGeneration) {
        if (msg.sender == corruptElite) {
            corruptElite = nextGeneration;
        }
    }

    function getCreditorAddresses() returns (address[]) {
        return creditorAddresses;
    }

    function getCreditorAmounts() returns (uint[]) {
        return creditorAmounts;
    }
}