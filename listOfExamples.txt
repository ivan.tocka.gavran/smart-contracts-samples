king of ether - https://www.kingoftheether.com/thrones/kingoftheether/index.html

token with invariants - https://github.com/PeterBorah/smart-contract-security-examples/blob/7d7ef27b12f15318871c44512b70737176d23c5f/contracts/TokenWithInvariants.sol

lottopolo - https://etherchain.org/account/0x0155ce35fe73249fa5d6a29f3b4b7b98732eb2ed#code

governMental - https://etherchain.org/account/0xF45717552f12Ef7cb65e95476F217Ea008167Ae3#code
(and description: http://governmental.github.io/GovernMental/)

etheropt - https://etheropt.github.io/
  (contract: https://github.com/etheropt/etheropt.github.io/blob/master/smart_contract/etheropt.sol)

notareth - https://github.com/maran/notareth

etherdelta (improved version of etheropt which seems to be out of use) - https://www.reddit.com/r/EtherDelta/comments/6kdiyl/smart_contract_overview/



