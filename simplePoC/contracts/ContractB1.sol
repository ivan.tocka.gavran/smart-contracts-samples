pragma solidity ^0.4.4;

//this is a simple contract illustrating basic communications between contracts and external accounts
contract ContractB1 
{
	
	bool public updateFunctionExecuted;
	event B1ReceivedSomeMoney();
	event Noise();
	event UpdateHappening();
	
	function ContractB1() 
	{
		updateFunctionExecuted = false;
	}
	
	function giveSomeNoise()
	{
		Noise();
	}
	
	function updateUsingTransfer()
	{
		UpdateHappening();
		updateFunctionExecuted = true;
		//msg.sender.call.value(1000000000000000000)();
		msg.sender.transfer(100000000000);
	}
	
	function updateUsingCall()
	{
		updateFunctionExecuted = true;
		msg.sender.call.value(1000000000000000000)();
		//msg.sender.transfer(100000000000);
	}
	
	function() payable
	{
		B1ReceivedSomeMoney();
	}
	
	function deposit() payable
	{
		B1ReceivedSomeMoney();
	}
	
}
