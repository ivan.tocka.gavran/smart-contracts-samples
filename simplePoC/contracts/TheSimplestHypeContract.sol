pragma solidity ^0.4.4;

//this is a simple contract illustrating basic communications between contracts and external accounts
contract TheSimplestHypeContract 
{
	
	int hypeLevel;
	event SomebodySendingMoney();
	event EnoughMoneySentForIncreasingHype();
	event NotEnoughMoneyTakingItAnyway();
	function HypeContract() 
	{
		hypeLevel = 10;
	}
	
	function() payable
	{
		
	}
	
	
	function donate() payable
	{
		if (msg.value > 20 ether)
		{
			increaseHype();
			EnoughMoneySentForIncreasingHype();
		}
		else 
			NotEnoughMoneyTakingItAnyway();
		
	}
	
	
	function increaseHype() internal
	{
		hypeLevel += 1;
	}
	
	function decreaseHype() internal
	{
		hypeLevel -= 1;
	}
}
