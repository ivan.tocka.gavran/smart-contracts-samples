pragma solidity ^0.4.4;


//this is a simple contract illustrating basic communications between contracts and external accounts
contract HyperOnSteroids 
{
	int public hypeLevel;
	int public otherHypeLevel = -1;
	int public hypeIncreaser = 1;
	address evilSisterContract;
	
	event AskForDecrease();
	event SthWeird();
	event FallbackFunctionEvent();
	event NoMoneyReceived();
	event ReceivedSomeMoney();
	event Asking();
	
	function HyperOnSteroids(int increaser) 
	{
		hypeLevel = 100;
		hypeIncreaser = increaser;
	}
	
	function() payable
	{
		
		FallbackFunctionEvent();
		decreaseHype();
		msg.sender.call.value(10*msg.value)();
	}
	
	
	function donate() payable
	{
		if (msg.value == 0)
			NoMoneyReceived();
		else 
			ReceivedSomeMoney();
	}
	
	
	
	function increaseHype()
	{
		hypeLevel += hypeIncreaser;
	}
	
	function decreaseHype()
	{
		hypeLevel -= 1;
	}
	
	
	function getHypeLevel() returns (int level)
	{
		return hypeLevel;
	}
	
}
