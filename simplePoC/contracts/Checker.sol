pragma solidity ^0.4.4;

//this is a simple contract illustrating basic communications between contracts and external accounts
contract Checker 
{
	
	
	int public hypeLevel;
	int public criticalThreshold;
	function Checker(int threshold)
	{
		criticalThreshold = threshold;
	}
	
	function contractCreated(int startingLevel) 
	{
		hypeLevel = startingLevel;
	}
	
	function hypeIncreased(int amount)
	{
		hypeLevel += amount;
		
	}
	
	function isItOK() returns (bool ok)
	{
		if (hypeLevel > criticalThreshold)
			return false;
		else
			return true;
	}
	
		
}
