pragma solidity ^0.4.4;


//

contract AntiHypeContract 
{
	
	address public hyper;
	event ReceivedSomeMoney();
	event NoMoneyReceived();
	function() payable
	{
		ReceivedSomeMoney();
	}
	function donate() payable
	{
		if (msg.value == 0)
			NoMoneyReceived();
		else 
			ReceivedSomeMoney();
	}
	
	
	// another option here would be to use transfer (but that would throw an exception)
	function sendMoney()
	{
		hyper.call.value(2000000000000000)();
	}
	
	//not checking for whether hyper fails (on purpose) -> and it should fail as the fallback function of a receiver does
	// more than simple logging so it would run out of gas
	function sendMoneyLimitedGas()
	{
		hyper.send(2000000000000000);
	}
	
	function AntiHypeContract(address existingHyperContract) 
	{
		hyper = existingHyperContract;
	}

	
}
