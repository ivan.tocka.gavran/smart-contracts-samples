var Checker = artifacts.require("./Checker.sol");
var ContractToCheck = artifacts.require("./ContractToCheck.sol");


contract('Checker', function(accounts) {
  
   // check whether the contract upon deployment has the right hype level
  it("testing the initial deploymend parameters", function() {
    return Checker.new(1).then(function(instance) {
      return instance.criticalThreshold.call()
      .then(function(threshold) {
    	  console.log("initial hype level = "+threshold.valueOf());
    	  assert.equal(threshold.valueOf(), 1, "1 wasn't the initial threshold");
      })
      .then(function(){
			return ContractToCheck.new(instance.address, 0)
			.then(function(checkedInstance){
				return checkedInstance.hypeLevel.call()	
				.then(function(level){
					assert.equal(level.valueOf(),0, "0 wans't hype level");
				});
			});
      });
    });
  });
  
  
  
  it("testing that nothing happens if the spec is satisfied", function() {
	    return Checker.new(1).then(function(instance) {
			return ContractToCheck.new(instance.address, 0)
			.then(function(checkedInstance){
				return checkedInstance.increaseHype(1)
				.then(function(){
					return checkedInstance.hypeLevel.call()
					.then(function(level){
						assert.equal(level.valueOf(), 1, "1 wasn't hype level");
					});
				});
			});
	    });
    });
  
  it("testing that exception is thrown if spec is not satisfied", function() {
	    return Checker.new(1).then(function(instance) {
			return ContractToCheck.new(instance.address, 0)
			.then(function(checkedInstance){
				console.log("about to increase hype");
				return checkedInstance.increaseHype(2)
				.then(function(returnValue) {
			      assert(false, "testThrow was supposed to throw but didn't.");
			    }).catch(function(error) {
			    	console.log("caught an error");
			    	//console.log(error);
			      if(error.toString().indexOf("invalid JUMP") != -1) {
			        console.log("We were expecting a Solidity throw (aka an invalid JUMP), we got one. Test succeeded.");
			      }
			       else if(error.toString().indexOf("invalid opcode") != -1) {
				        console.log("We got invalid opcode. Test succeeded.");
			       }
				   else {
			        // if the error is something else (e.g., the assert from previous promise), then we fail the test
			        assert(false, error.toString());
			      }
			    });
			});
	    });
  });
	  
  
  
  
	

});


