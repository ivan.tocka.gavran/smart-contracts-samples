var HypeContract = artifacts.require("./HypeContract.sol");
var AntiHypeContract = artifacts.require("./AntiHypeContract.sol");


contract('HypeContract', function(accounts) {
  
   // check whether the contract upon deployment has the right hype level
  it("testing the initial hype level", function() {
    return HypeContract.deployed(2).then(function(instance) {
      return instance.getHypeLevel.call();
    }).then(function(level) {
      assert.equal(level.valueOf(), 10, "10 wasn't the initial hype level");
    });
  });
  
  it("testing other hype level initially", function(){
	 return HypeContract.deployed().then(function(instance){
		return instance.otherHypeLevel().then(function(level){
			assert.equal(level.valueOf(), -1, "-1 wasn't initially there");
		}); 
	 });
  });
  
//   the scenario: a contract (Hyper) is created. afterwards, another one is created (AntiHyper). AntiHyper knows the address of Hyper
//  (as it is created later). it sends some small amount of money to Hyper that afterwards executes its fallback function
//  (which is in this case decreasing hype and sending 10 times the amount of money it received).
  // the differences between call and send operations are described in https://ethereum.stackexchange.com/questions/6470/send-vs-call-differences-and-when-to-use-and-when-not-to-use
  it("creating a contract, creating another one and sending some money using function sendMoney (that has more gas)", function() {
	    return HypeContract.deployed()
	    .then(function(instance){
	    	return instance.donate({from:accounts[0], to: instance.address, value: web3.toWei(4, "ether")})
	    	.then(function(){
		    	console.log("initial ether at external account 0: " + web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
		    	console.log("initial ether at hyper contract: "+web3.fromWei(web3.eth.getBalance(instance.address), 'ether').toNumber());
		    	return AntiHypeContract.new(instance.address).then(function(antiHypeInstance){
		    		return antiHypeInstance.donate({from:accounts[0], to: antiHypeInstance.address, value: web3.toWei(5, "ether")})
			    	.then(function(){
			    		console.log("ether at external account 0 after the transaction: "+web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
			    		console.log("ether at anti-hyper contract after the transaction "+web3.fromWei(web3.eth.getBalance(antiHypeInstance.address), 'ether').toNumber());
			    		return antiHypeInstance.hyper();
			    	})
			    	.then(function(hyperAddress){
			    		return antiHypeInstance.sendMoney();
			    	})	
			    	.then(function(){
			    		console.log("------");
			    		console.log("situation after anti hyper sent some money to hyper (which in turn cause hyper to send some money back)");
			    		console.log("external account 0: "+web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
			    		console.log("anti hyper: "+web3.fromWei(web3.eth.getBalance(antiHypeInstance.address), 'ether').toNumber());
			    		console.log("hyper: "+web3.fromWei(web3.eth.getBalance(instance.address), 'ether').toNumber());
			    		return instance.getHypeLevel.call();
		    		})
		    		.then(function(level){
		    			console.log("hype level at hyper: "+level.valueOf());
		    		})
		    		
		    	});
		    });
	    });
  });
  
}); 

contract('HypeContract', function(accounts) {
//  the scenario: a contract (Hyper) is created. afterwards, another one is created (AntiHyper). AntiHyper knows the address of Hyper
//(as it is created later). it sends some small amount of money to Hyper using the send function that only has limited gas
//  that afterwards executes its fallback function
//(which is in this case decreasing hype and sending 10 times the amount of money it received).
// the differences between call and send operations are described in https://ethereum.stackexchange.com/questions/6470/send-vs-call-differences-and-when-to-use-and-when-not-to-use
	it("creating a contract, creating another one and sending some money using function sendMoneyLimitedGas (that has limited amount of gas)", function() {
	    return HypeContract.deployed()
	    .then(function(instance){
	    	return instance.donate({from:accounts[0], to: instance.address, value: web3.toWei(4, "ether")})
	    	.then(function(){
		    	console.log("initial ether at external account 0: " + web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
		    	console.log("initial ether at hyper contract: "+web3.fromWei(web3.eth.getBalance(instance.address), 'ether').toNumber());
		    	return AntiHypeContract.new(instance.address).then(function(antiHypeInstance){
		    		return antiHypeInstance.donate({from:accounts[0], to: antiHypeInstance.address, value: web3.toWei(5, "ether")})
			    	.then(function(){
			    		console.log("ether at external account 0 after the transaction: "+web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
			    		console.log("ether at anti-hyper contract after the transaction "+web3.fromWei(web3.eth.getBalance(antiHypeInstance.address), 'ether').toNumber());
			    		return antiHypeInstance.hyper();
			    	})
			    	.then(function(hyperAddress){
			    		return antiHypeInstance.sendMoneyLimitedGas();
			    	})	
			    	.then(function(){
			    		console.log("------");
			    		console.log("situation after anti hyper sent some money to hyper (which in turn cause hyper to send some money back)");
			    		console.log("external account 0: "+web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether').toNumber());
			    		console.log("anti hyper: "+web3.fromWei(web3.eth.getBalance(antiHypeInstance.address), 'ether').toNumber());
			    		console.log("hyper: "+web3.fromWei(web3.eth.getBalance(instance.address), 'ether').toNumber());
			    		return instance.getHypeLevel.call();
		    		})
		    		.then(function(level){
		    			console.log("hype level at hyper: "+level.valueOf());
		    		})
		    		
		    	});
		    });
	    });
  });  
  
});


