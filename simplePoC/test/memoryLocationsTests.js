var HypeContract = artifacts.require("./HypeContract.sol");
var AntiHypeContract = artifacts.require("./AntiHypeContract.sol");
var HyperOnSteroids = artifacts.require("./HyperOnSteroids.sol");


contract('HypeContract', function(accounts) {
  
   // check whether the contract upon deployment has the right hype level
  it("testing the initial hype level", function() {
    return HypeContract.deployed().then(function(instance) {
      return instance.getHypeLevel.call();
    }).then(function(level) {
    	console.log("initial hype level = "+level.valueOf());
      assert.equal(level.valueOf(), 10, "10 wasn't the initial hype level");
    });
  });

  // illustrating that when dealing with ints (expected: anything except structs and arrays
  it("testing what happens when messing with storage int variable", function(){
	 return HypeContract.deployed().then(function(instance){
		return instance.getHypeLevel.call()
		 .then(function(level){
			console.log("initial hype level = "+level.valueOf());
			return instance.messUpWithStorage();
		 }).then(function(){
			return instance.getHypeLevel.call(); 
		 }).then(function(level){
			console.log("hype level after messing with storage = "+level.valueOf()); 
		 });
	 });
  });
 	 
  
  it("testing what happens when messing with storage struct variable", function(){
		 return HypeContract.deployed().then(function(instance){
			return instance.info.call()
			 .then(function(info){
				console.log("initial struct info = "+info);
				return instance.alternativeInfo.call()
			 }).then(function(altInfo){
				 console.log("initial struct alternativeInfo = "+altInfo);
				 return instance.messUpWithStructStorage();
			 }).then(function(){
				return instance.info.call(); 
			 }).then(function(infoPostFestum){
				console.log("struct info after messing with storage = "+infoPostFestum); 
				return instance.alternativeInfo.call()
			 }).then(function(altInfoPostFestum){
				console.log("struct alternative info after messing with storage = "+altInfoPostFestum);
			});
		 });
	  });
	

});


