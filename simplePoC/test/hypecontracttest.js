var HypeContract = artifacts.require("./HypeContract.sol");
var AntiHypeContract = artifacts.require("./AntiHypeContract.sol");
var HyperOnSteroids = artifacts.require("./HyperOnSteroids.sol");


contract('HypeContract', function(accounts) {
  
	it("only deploymenr", function(){
		return HypeContract.deployed();
	});
	
   // check whether the contract upon deployment has the right hype level
  it("testing the initial hype level", function() {
	  // HypeContract is deployed here and that's all there is (deployed by a testing framework)
	  // - somewhat artificial. 
    return HypeContract.deployed().then(function(instance) {
    	// this is an read-only, call (and not transaction).
//         - doesn't cost any gas
//         - doesn't change anything in the blockchain
//         - processed immediately
//    	as explaine at http://truffleframework.com/docs/getting_started/contracts#calls
      return instance.getHypeLevel.call();
    }).then(function(level) {
      assert.equal(level.valueOf(), 10, "10 wasn't the initial hype level");
    });
  });

  


  	  // run a function on the contract and check whether the hype level increased
	  it("testing the increaseHype functions", function() {
	    return HypeContract.deployed().then(function(instance) {
	      return instance.increaseHype().then(function(){
	    	  return instance.getHypeLevel.call().then(function(hypeLevel){
	    		 console.log(hypeLevel.valueOf());
	    		 assert.equal(hypeLevel.valueOf(), 11, "hype didn't increased to 11")
	    	  });
	      });
	      
	    });
	  });
	  
	  // here the HypeContract creates a new contract (HyperOnSteroids)
	  it("testing creation of new contract from the contract", function(){
		 return HypeContract.deployed().then(function(instance){
			return instance.createEvilTwinSister()
			.then(function(){
				return instance.evilSisterContract.call();
			}).then(function(address){
				return HyperOnSteroids.at(address);
			}).then(function(hyperOnSteroidsInstance){
				return hyperOnSteroidsInstance.hypeLevel.call()
				.then(function(level){
				console.log("hype level of the evil twin after creation: "+level.valueOf());
				return hyperOnSteroidsInstance.increaseHype();
				}).then(function(){
					return hyperOnSteroidsInstance.hypeLevel.call();
				}).then(function(level){
					console.log("hype level of the evil twin after increasing: "+level.valueOf());
					assert.equal(level.valueOf(), 110, "20 wasn't the hype level");
				});
			});
				
		});
});
	  
	  
	  
	

});


