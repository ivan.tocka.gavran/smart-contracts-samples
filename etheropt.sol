// this is just hopeless - no comments and meaning is contained in the variable names, that is jargon I can't follow easily.
// anyway, could this actually be a proper motivation for having some requirements based on description and then having no need to understand the code, but the tool should do it for us?
// the problem is, though, that I don't understand the logic, principles, behind it :(
//not active anymore - people are not trading with it, everything moved to etherdelta

//after some more digging, I think I finally have at least some grasp of it
//last compiled with soljson-v0.3.6-2016-08-29-b8060c5.js

//let's see what makes it safe actually :) --> it's assert statement that in the case of any bug throws and reverts the state. while require is used to check user's input of any kind, assert is there to
// protect the code from the code's own problematic parts. except that here they define their own assert (although solidity documentation mentions 
//assert as keyword http://solidity.readthedocs.io/en/develop/control-structures.html#error-handling-assert-require-revert-and-exceptions)

//
// well, even though I am not completely clear about every bit of this contract, I think I can imagine writing down some requirements users could have. The assumption here is that I don't check for frontend
// but assume a user who would deal with this smart contract himself
//
//  - if I have ordered a position for x ether, I will either spend x ether and get the position, or not spend any money (except for the gas cost)
//  - if I have bought a call position for x, I will loose at most x ether
/// - if I have bought a position for call y with margin m, and the price ends up at y + k, I will get min(k, m) ether
//  - if I have sold a position for put/call y with margin m for price x ether, I will loose at most (m-x) ether
//  - if I have bought a position for put y with margin m and the price ends up at y - k, I get min(k,m) ether
//  - never will the position transfer be allowed if in the future one of the parties can't cover it's expenses
//  - if in the final summary I would be able to pay all my debts, the transaction would be allowed (that's not true, the function checks it rather conservatively)
//  - at the expiration date, all positions will be exercised and all the traders will get their money (if I understood the contract correctly, this is somewhat problematic as the trigger for expiry is the person who set the expirations)
//  - after the expire function is executed, the etheropt contract will not have any money left
//  - if the order is canceled, it will never be executed (this is maybe the most problematic case, I really don't get the code behind it)

contract SafeMath {
  //internals

  function safeMul(uint a, uint b) internal returns (uint) {
    uint c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function safeSub(uint a, uint b) internal returns (uint) {
    assert(b <= a);
    return a - b;
  }

  function safeAdd(uint a, uint b) internal returns (uint) {
    uint c = a + b;
	//wild checks (overflow?), I guess to build as much trust with users as possible
    assert(c>=a && c>=b);
    return c;
  }

  function safeMuli(int a, int b) internal returns (int) {
    int c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function safeSubi(int a, int b) internal returns (int) {
    int negB = safeNegi(b);
    int c = a + negB;
    if (b<0 && c<=a) throw;
    if (a>0 && b<0 && c<=0) throw;
    if (a<0 && b>0 && c>=0) throw;
    return c;
  }

  function safeAddi(int a, int b) internal returns (int) {
    int c = a + b;
    if (a>0 && b>0 && c<=0) throw;
    if (a<0 && b<0 && c>=0) throw;
    return c;
  }

  function safeNegi(int a) internal returns (int) {
    int c = -a;
    if (a<0 && -a<=0) throw;
    return c;
  }

  function safeIntToUint(int a) internal returns(uint) {
    uint c = uint(a);
    assert(a>=0);
    return c;
  }

  function safeUintToInt(uint a) internal returns(int) {
    int c = int(a);
    assert(c>=0);
    return c;
  }

  function assert(bool assertion) internal {
    if (!assertion) throw;
  }
}


//etheropt inherits from safeMath
contract Etheropt is SafeMath {

  uint public version = 2;

  // still not clear what is the logic behind Position struct. 
  struct Position {
    mapping(uint => int) positions;
    int cash;
    bool expired;
    bool hasPosition;
  }
  uint public expiration;
  string public underlying; // if it's a string it could be ETH/USD, it generally means the value for which the options are traded
  uint public margin; // margin is generally security that the option dealer has to deposit to be sure to meet his obligations
  int public unit = 1000000000000000000; //10**18 wei = 1 ether
  uint public realityID; //no clue
  bytes32 public factHash; // still less idea what that is
  address public ethAddr; //address (of the options issuer, hopefully, but not completely sure)
  mapping(uint => int) options; //somehow value of this mapping should represent call/put (probably through sign) and the option value (maybe that would be the strike)
  uint public numOptions;
  bool public expired;
  mapping(address => Position) positions;
  uint public numPositions;  //other people's positions i the contract? 
  uint public numPositionsExpired;

  //user's account -> determined by his address and keeps track of the capital available
  struct Account {
    address user;
    int capital;
  }
  mapping(bytes32 => int) orderFills; //keeps track of cumulative order fills
  mapping(uint => Account) accounts; //accounts of all users (having options here, I would guess)
  uint public numAccounts;
  mapping(address => uint) accountIDs; //starts at 1 --ivan: every account has its internal id, as it seems

  //events
  event Deposit(address indexed user, uint amount, int balance); //balance is balance after deposit
  event Withdraw(address indexed user, uint amount, int balance); //balance is balance after withdraw
  event Expire(address indexed caller, address indexed user); //user is the account that was expired --ivan: still don't get how a single account expires...
  event OrderMatch(address indexed matchUser, int matchSize, address indexed orderUser, int orderSize, uint optionID, uint price);
  event Order(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, address addr, uint8 v, bytes32 r, bytes32 s);
  event Cancel(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, address addr, uint8 v, bytes32 r, bytes32 s);

  // constructor - as far as I understand, there is a separate smart contract for every expiration created (for every options offering)
  // length of array strikes is also the total number of options offered
  function Etheropt(uint expiration_, string underlying_, uint margin_, uint realityID_, bytes32 factHash_, address ethAddr_, int[] strikes_) {
    expiration = expiration_; 
    underlying = underlying_;
    margin = margin_; 
    realityID = realityID_;
    factHash = factHash_;
    ethAddr = ethAddr_;
    for (uint i=0; i < strikes_.length; i++) {
		// in reality it is bound to 20?? where does exactly this number come from? hm... on the frontend they offer place for only 5 call and 5 put options. maybe this inconsisteny has
		// to do with changes that happened on frontend at some point?
      if (numOptions<20) {
        uint optionID = numOptions++;
        options[optionID] = strikes_[i];  //confirmed from frontend, strike > 0 is call option, strike < 0 is a put option
      }
    }
  }

  // function being constant means it can't (promises not to?) modify the state
  function getAccountID(address user) constant returns(uint) {
    return accountIDs[user];
  }

  function getAccount(uint accountID) constant returns(address) {
    return accounts[accountID].user;
  }

  // adding funds to personal account
  // could this adding funds be in any way problematic since it increases the array without control? (subsequent deleting might cause some trouble... maybe)
  // also, it seems that a single account is connected to single expiration available :)
  function addFunds() {
    if (accountIDs[msg.sender]>0) {
      accounts[accountIDs[msg.sender]].capital = safeAddi(accounts[accountIDs[msg.sender]].capital, safeUintToInt(msg.value));
    } else {
      uint accountID = ++numAccounts;
      accountIDs[msg.sender] = accountID;
      accounts[accountID].user = msg.sender;
      accounts[accountID].capital = safeAddi(accounts[accountID].capital, safeUintToInt(msg.value));
    }
	//firing an event in the end so people can follow the deposit happened
    Deposit(msg.sender, msg.value, accounts[accountIDs[msg.sender]].capital);
  }

  // this one at least seems reasonable :)
  // any user can withdraw funds. It's still not clear, though, how this is done on frontend
  // however, it's clear that anybody can use it publicly
  function withdrawFunds(uint amount) {
    if (accountIDs[msg.sender]<=0) throw;
    int amountInt = safeUintToInt(amount);
    if (amountInt>getFunds(msg.sender, true) || amountInt<=0) throw;
    accounts[accountIDs[msg.sender]].capital = safeSubi(accounts[accountIDs[msg.sender]].capital, amountInt);
	//send amount (wei) to msg.sender with no payload. if the sending doesn't succeed, throw
    if (!msg.sender.call.value(amount)()) throw;
    Withdraw(msg.sender, amount, accounts[accountIDs[msg.sender]].capital);
  }

  function getFunds(address user, bool onlyAvailable) constant returns(int) {
    if (accountIDs[user]<=0) return 0;
    if (onlyAvailable == false) {
      return accounts[accountIDs[user]].capital;
    } else { // it is not perfectly clear what onlyAvailable stands for, I would assume an option on whether to return what are funds taken and what is the worst-case situation
      return safeAddi(accounts[accountIDs[user]].capital, getMaxLossAfterTrade(user, 0, 0, 0));
    }
  }

  //publishes the Order event, but what on top of it? rejects any payments, that's fine, but what else is happening? not clear... also, usually they would define function as constant in order to avoid changing state
  // why not here as well?
  // aha: order is put and then whoever likes it tries to Match it... finally I see
  function order(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, uint8 v, bytes32 r, bytes32 s) {
    if (msg.value>0) throw;
    Order(contractAddr, optionID, price, size, orderID, blockExpires, msg.sender, v, r, s);
  }


  //again, it doesn't seem to do much more than publishing the event is cancelling order makes sense... maybe somebody (inside this contract) subscribes to that event?
  function cancelOrder(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, uint8 v, bytes32 r, bytes32 s) {
  //always checking whether some money was sent and rejecting it
    if (msg.value>0) throw;
	//sha256 is natively available in solidity
    bytes32 hash = sha256(contractAddr, optionID, price, size, orderID, blockExpires);
	//ecrecover is to getting the address from once public key
    if (ecrecover(hash,v,r,s) != msg.sender) throw;
	// checking that it's not about some other contract
    if (contractAddr != address(this)) throw;
    orderFills[hash] = size;
    Cancel(contractAddr, optionID, price, size, orderID, blockExpires, msg.sender, v, r, s);
  }
  //funds are total funds, and available is total funds minus maximal possible loss
  function getFundsAndAvailable(address user) constant returns(int, int) {
    return (getFunds(user, false), getFunds(user, true));
  }

  function getOptionChain() constant returns (uint, string, uint, uint, bytes32, address) {
    return (expiration, underlying, margin, realityID, factHash, ethAddr);
  }

  function getMarket(address user) constant returns(uint[], int[], int[], int[]) {
    uint[] memory optionIDs = new uint[](20);
    int[] memory strikes_ = new int[](20);
    int[] memory positions_ = new int[](20);
    int[] memory cashes = new int[](20);
    uint z = 0;
    if (expired == false) {
      for (uint optionID=0; optionID<numOptions; optionID++) {
        optionIDs[z] = optionID;
        strikes_[z] = options[optionID];
        positions_[z] = positions[user].positions[optionID];
        cashes[z] = positions[user].cash;
        z++;
      }
    }
	//returns all user's positions in all available options
    return (optionIDs, strikes_, positions_, cashes);
  }
  

  //expiring the option? or expiring the account? (maybe the accountId==0 means that all accounts should be "expired")
  // it seems that lots of logic is contained in the frontend file as well (that would explain some the weirdness of this function)
  // from the frontend: it always sends 0 as the first argument (accountID)
  //valule is the value of the trade as reported by some service and called from frontend

  function expire(uint accountID, uint8 v, bytes32 r, bytes32 s, bytes32 value) {
	// if I got it correctly, ethAddr is the address that created this option contracts (it is for sure present in the constructor)
	// so, only the author of the contract can expire the position. however, there is nowhere the expiry date considerred. that means that lots of trust has to be placed in the author of the option... :/
    if (expired == true || ecrecover(sha3(factHash, value), v, r, s) != ethAddr) throw; // facthash is something created at the beginning of the expiry. does this somehow includes info about the date?
    uint lastAccount = numAccounts;
    if (accountID==0) {
      accountID = 1;
    } else {
      lastAccount = accountID;
    } 

	//iterates over account to send some money to contracts
    for (accountID=accountID; accountID<=lastAccount; accountID++) {
      if (positions[accounts[accountID].user].expired == false) {
        int result = positions[accounts[accountID].user].cash / unit;
        for (uint optionID=0; optionID<numOptions; optionID++) {
          int moneyness = getMoneyness(options[optionID], uint(value), margin);
		  // final result is number of positions multiplied by the money one should get per position
          result += moneyness * positions[accounts[accountID].user].positions[optionID] / unit; 
        }
		// mark this particular position as expired
        positions[accounts[accountID].user].expired = true; 

		// send to the user his remaining capital and the result from the option bet
        uint amountToSend = safeIntToUint(safeAddi(accounts[accountID].capital, result));
        accounts[accountID].capital = 0;
        if (positions[accounts[accountID].user].hasPosition==true) {
          numPositionsExpired++;
        }

		// finally, send the money (amount to send) without additional payload
        accounts[accountID].user.call.value(amountToSend)();
		//finally, publish the event that this particular account expired
        Expire(msg.sender, accounts[accountID].user);
      }
    }
    if (numPositionsExpired == numPositions) {
      expired = true;
    }
  }

  //this is probably the crucial function of the code
  // strike is probably defining whether we're talking about call or put option
  //settlement here is the value of final trading

  function getMoneyness(int strike, uint settlement, uint margin) constant returns(int) {
    // explanation as a reminder: call option is the one when I as a buyer pay certain smaller amount of money to bet that the stock would go up. If it really goes up, the option seller has to pay me 
	// the difference (he is, on the other hand, protected by the upper bound, called margin here: he pays min(difference, margin)). If the stock goes down, nothing happens (I loose my initial
	// small amount of money)
    if (strike>=0) { //call

	  //if the final value is greater than the price that I "reserved"
      if (settlement>safeIntToUint(strike)) {
	    // it it is greater, but still in the security margin by the option seller, less than "in-the-money" limit, return the difference
        if (safeSub(settlement,safeIntToUint(strike))<margin) {
          return safeUintToInt(safeSub(settlement,safeIntToUint(strike)));
        } else { // in case the price really plummeted, return the max promised gain that the option seller promise
          return safeUintToInt(margin); 
        }
      } else { // if on the other hand final value was smaller than the strike, the user gets nothing
        return 0;
      }
    } else { //put. explanation as a reminder: I bet that the stock (here, ETH/USD) will go down (lower than strike value). if it really goes down the option seller pays me the difference (again, protected by the margin).
	// otherwise, nothing happens and I loose my initial bet-money
      if (settlement<safeIntToUint(safeNegi(strike))) { // if final value ended up below the offered by the option seller
        if (safeSub(safeIntToUint(safeNegi(strike)),settlement)<margin) { // it it was still in the margin borders
          return safeUintToInt(safeSub(safeIntToUint(safeNegi(strike)),settlement)); // I get the difference from strike
        } else { // if it is not in the margin-protected-area
          return safeUintToInt(margin); // return the margin (so that the option seller is protected as well)
        }
      } else {
        return 0;
      }
    }
  }

  //trying to match one's order to offered orders
  // the order match test is (as visible from the frontend) called only locally. if it returns true, only then does the user proceed to actual order matching (that still can fail due to somebody else //
  //buying the option. however, this way number of useless transactions is reduced)
  function orderMatchTest(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, address addr, address sender, uint value, int matchSize) constant returns(bool) {

    // if somebody specified one address, but sent to another (if using frontend, that wouldn't happen. however, it is good to have a protection like this)
    if (contractAddr != address(this)) return false;

	//unable  to understand from frontend what is matchSize and what is size :/
    if (block.number>blockExpires) return false;
    if (size>0 && matchSize>0) return false;
    if (size<0 && matchSize<0) return false;
    if (size>0 && matchSize<0 && orderFills[sha256(contractAddr, optionID, price, size, orderID, blockExpires)]>safeAddi(size,matchSize)) return false;
    if (size<0 && matchSize>0 && orderFills[sha256(contractAddr, optionID, price, size, orderID, blockExpires)]<safeAddi(size,matchSize)) return false;
    if (getFunds(addr, false)+getMaxLossAfterTrade(addr, optionID, -matchSize, matchSize * safeUintToInt(price))<=0) return false;
    if (getFunds(sender, false)+int(value)+getMaxLossAfterTrade(sender, optionID, matchSize, -matchSize * safeUintToInt(price))<=0) return false;
    return true;
  }

  function getOrderFill(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires) constant returns(int) {
    return size-orderFills[sha256(contractAddr, optionID, price, size, orderID, blockExpires)];
  }

  function orderMatch(address contractAddr, uint optionID, uint price, int size, uint orderID, uint blockExpires, address addr, uint8 v, bytes32 r, bytes32 s, int matchSize) {
    addFunds();
    bytes32 hash = sha256(contractAddr, optionID, price, size, orderID, blockExpires);
    if (ecrecover(hash, v, r, s) != addr) throw;
    if (contractAddr != address(this)) throw;
    if (block.number>blockExpires) throw;
    if (size>0 && matchSize>0) throw;
    if (size<0 && matchSize<0) throw;
    if (size>0 && matchSize<0 && orderFills[hash]>safeAddi(size,matchSize)) throw;
    if (size<0 && matchSize>0 && orderFills[hash]<safeAddi(size,matchSize)) throw;
    if (getFunds(addr, false)+getMaxLossAfterTrade(addr, optionID, -matchSize, matchSize * safeUintToInt(price))<=0) throw;
    if (getFunds(msg.sender, false)+getMaxLossAfterTrade(msg.sender, optionID, matchSize, -matchSize * safeUintToInt(price))<=0) throw;
    if (positions[msg.sender].hasPosition == false) {
      positions[msg.sender].hasPosition = true;
      numPositions++;
    }
    if (positions[addr].hasPosition == false) {
      positions[addr].hasPosition = true;
      numPositions++;
    }
    positions[msg.sender].positions[optionID] = safeAddi(positions[msg.sender].positions[optionID], matchSize);
    positions[msg.sender].cash = safeSubi(positions[msg.sender].cash, safeMuli(matchSize, safeUintToInt(price)));
    positions[addr].positions[optionID] = safeSubi(positions[addr].positions[optionID], matchSize);
    positions[addr].cash = safeAddi(positions[addr].cash, safeMuli(matchSize, safeUintToInt(price)));
    orderFills[hash] = safeSubi(orderFills[hash], matchSize);
    OrderMatch(msg.sender, matchSize, addr, size, optionID, price);
  }

  
  // this function is called all around the place, let's understand what it does
  // still confused but it seems to be giving max loss over all options that are offered
  // it doesn't seem to take into account all the combination, but just the max loss... hm, let's check this one more time
  function getMaxLossAfterTrade(address user, uint optionID, int positionChange, int cashChange) constant returns(int) {
    bool maxLossInitialized = false;
    int maxLoss = 0;
    if (positions[user].expired == true || numOptions<=0) return 0; // so far so good, if the position expired or the user doesn't have any options this user gets nothing
    
	for (uint s=0; s<numOptions; s++) {
      int pnl = positions[user].cash / unit;  //user's cash in ethers
      pnl = safeAddi(pnl, cashChange / unit); // if the trade happens, what would be the amount user would have
      uint settlement = 0;
	  // making sure that settlement is positive number (the sign here only determines whether it is a call or a put option)
      if (options[s]<0) {
        settlement = safeIntToUint(safeNegi(options[s]));
      } else {
        settlement = safeIntToUint(options[s]);
      }
	  //how much money user would get if the settlement happened
      pnl = safeAddi(pnl, moneySumAtSettlement(user, optionID, positionChange, settlement));
      if (pnl<maxLoss || maxLossInitialized==false) {
        maxLossInitialized = true;
        maxLoss = pnl;
      }
      pnl = positions[user].cash / unit;
      pnl = safeAddi(pnl, cashChange / unit);
      settlement = 0;
	  //here check for settlements that end up in max loss/gain
      if (options[s]<0) {
        if (safeIntToUint(safeNegi(options[s]))>margin) {
          settlement = safeSub(safeIntToUint(safeNegi(options[s])), margin);
        } else {
          settlement = 0;
        }
      } else {
        settlement = safeAdd(safeIntToUint(options[s]), margin);
      }
      pnl = safeAddi(pnl, moneySumAtSettlement(user, optionID, positionChange, settlement));
      if (pnl<maxLoss) {
        maxLoss = pnl;
      }
    }
    return maxLoss;
  }

  function moneySumAtSettlement(address user, uint optionID, int positionChange, uint settlement) internal returns(int) {
    int pnl = 0;
    for (uint j=0; j<numOptions; j++) {
      pnl = safeAddi(pnl, safeMuli(positions[user].positions[j], getMoneyness(options[j], settlement, margin)) / unit);
      if (j==optionID) {
        pnl = safeAddi(pnl, safeMuli(positionChange, getMoneyness(options[j], settlement, margin)) / unit);
      }
    }
    return pnl;
  }

  function min(uint a, uint b) constant returns(uint) {
    if (a<b) {
      return a;
    } else {
      return b;
    }
  }
}