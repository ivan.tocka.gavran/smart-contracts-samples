pragma solidity ^0.4.4;

contract Puzzle
{
	address public owner;
	bool public locked;
	uint public reward;
	uint public diff; // stands for difficulty
	uint public solution;

	function Puzzle() payable
	{
		owner = msg.sender;
		reward = msg.value;
		locked = false;
		diff = 27;
	}

	function checkTheSolution(uint proposedSolution) payable returns (bool) //somebody submited the solution, we want to check it
	{
		
		
		if(locked)
		{
			throw;
		}
		if (proposedSolution * proposedSolution < diff)
		{
			msg.sender.send(reward); // transfer handles failures automatically
			solution = proposedSolution;
			locked = true;
			return true;
		}
		else
		{
			return false;
		}
		
	}

	function updateTheReward() payable
	{
		if ( msg.sender != owner )
		{
			throw;
		}

		owner.send(reward);
		reward = msg.value;
	}

}