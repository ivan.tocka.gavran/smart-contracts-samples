var Puzzle = artifacts.require("./Puzzle.sol");

contract('Puzzle', function (accounts) {
    it("should test the constructor of Puzzle contract for difficulty", function () {
        return Puzzle.deployed().then(function (instance) { 
            //console.log(instance.locked);
            var diffLevel = instance.diff.call();
            console.log(diffLevel);
            return diffLevel; 

        }).then(function (difficulty) {
            console.log(difficulty);
            assert.equal(difficulty.valueOf(), 27, "27 wasn't the difficulty in the first account");
        });
    });

    it("should test the constructor of Puzzle contract for value", function () {
        return Puzzle.deployed().then(function (instance) { 
            //console.log(instance.locked);
            var reward = instance.reward.call();
            console.log(reward);
            return reward;

        }).then(function (reward) {
            console.log(reward);
            assert.equal(reward.valueOf(), 0, "0 wasn't the difficulty in the first account");
        });
    });

    
});
