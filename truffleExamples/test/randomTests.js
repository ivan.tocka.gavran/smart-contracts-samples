var MetaCoin = artifacts.require('./MetaCoin.sol')

contract('RandomMetaCoinTest', function (accounts) {
    it("testing logging", function () {
        console.log("kak si? pa tak");
        MetaCoin.deployed().then(function (instance) {
            console.log("deployao");
        }).then(function (instance) {
            console.log("ide");
        });
        
    });

    it("should fail when sending coins", function () {
        var meta;

        // Get initial balances of first and second account.
        var account_one = accounts[0];
        var account_two = accounts[1];

        var account_one_starting_balance;
        var account_two_starting_balance;
        var account_one_ending_balance;
        var account_two_ending_balance;

        var amount = 10000;

        return MetaCoin.deployed().then(function (instance) {
            meta = instance;
            return meta.getBalance.call(account_one);
        }).then(function (balance) {
            account_one_starting_balance = balance.toNumber();
            return meta.getBalance.call(account_two);
        }).then(function (balance) {
            account_two_starting_balance = balance.toNumber();
            return meta.sendCoin(account_two, amount, { from: account_one });
        }).then(function () {
            return meta.getBalance.call(account_one);
        }).then(function (balance) {
            account_one_ending_balance = balance.toNumber();
            return meta.getBalance.call(account_two);
        }).then(function (balance) {
            account_two_ending_balance = balance.toNumber();

            assert.equal(account_one_ending_balance, account_one_starting_balance, "Due to invalid transaction expected no change, however the change happened");
            assert.equal(account_two_ending_balance, account_two_starting_balance, "Due to invalid transaction expected no change, however the change happened");
        });
    });
})